
<?php

return array(
	'label' => array('Pop-up Nachricht', 'Nachricht welche beim Laden der Seite erscheint'),
	'types' => array('content'),
	'contentCategory' => 'MEMO Customs',
	'standardFields' => array('headline', 'cssID'),
	'fields' => array(
		'text' => array(
			'label' => array(
				'Text', 
				'Details zur Nachricht'
			),
			'inputType' => 'textarea',
			'eval' => array(
				'rte' => 'tinyMCE'
			),
		),
		'expiration' => array(
			'label' => array(
				'Ablauf', 
				'Cookie läuft nach dieser Zeit ab'
			),
			'inputType' => 'text',
			'eval' => array(
				'mandatory' => true,
				'tl_class' => 'w50'
			),
		),
		'time_format' => array(
			'label' => array(
				'Format', 
				''
			),
			'inputType' => 'select',
			'options' => array(
				'days' => 'Tage',
				'hours' => 'Stunden',
				'minutes' => 'Minuten',
				'seconds' => 'Sekunden'
			),
			'eval' => array(
				'mandatory' => true,
				'tl_class' => 'w50'
			),
		),
	),
);
