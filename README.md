#CustomElement für Pop-up

##Funktionen
* Öffnet beim Laden der Seite ein Pop-up mit Nachricht
* Titel und Text der Nachricht können im Backend definiert werden
* Wenn User die Nachricht schliesst, wird ein Local Storage gespeichert und die Nachricht erscheint solange nicht mehr
* Die Ablaufzeit des Cookies kann ebenfalls im Backend definiert werden

##Installation
* Templates hochladen
* CustomElements muss installiert sein
* Inhaltselement in Artikel einfügen und Felder ausfüllen
* Falls das Pop-up auf jeder Seite aktiv sein soll
    * Modul mit Inserttag zum entsprechendem Inhaltselement erstellen
    * Modul in allen Seitenlayouts einbinden
* Im Seitenlayout, auf welchem das Pop-up aktiv ist, muss j_colorbox aktiviert sein

##Sonstiges
* Das Javascript wird im Template geschrieben
* Beim Ausblenden des betreffenden Artikels/Inhaltselementes, wird die Pop-up Nachricht nicht erscheinen
    * Wenn Javascript in ein separates File übernommen wird, sind dazu noch Anpassungen nötig